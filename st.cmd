require xtpico
require essioc

iocshLoad("$(essioc_DIR)common_config.iocsh")

epicsEnvSet("PREFIX",               "$(PREFIX=LabS-RFLab:RFS-ED-001)")
epicsEnvSet("DEVICE_IP",            "rf-lab-ep1.cslab.esss.lu.se")

epicsEnvSet("I2C_COMM_PORT",        "AK_I2C_COMM")
epicsEnvSet("R_TMP100",             ":Temp")
epicsEnvSet("R_M24M02",             ":Eeprom")
epicsEnvSet("R_TCA9555",            ":IOExp")
epicsEnvSet("R_LTC2991",            ":VMon")
epicsEnvSet("R_AD527X",             ":Res")

iocshLoad("$(xtpico_DIR)e-pickup.iocsh")

afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirPin0 1")    # Input : Test Monitoring
afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirPin1 1")    # Input : Interlock OFF
afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirPin2 1")    # Input : Interlock ON
afterInit("dbpf $(PREFIX)$(R_TCA9555)-DirPin7 0")    # Output: test LED

iocInit()
